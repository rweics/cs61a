def square(x):
    return x * x

def sum(n):
    if n == 0:
        return n
    else:
        return n+sum(n-1)

def ab_plus_c(a, b, c):
    if a == 0:
        return c
    else:
        return b + ab_plus_c(a-1, b, c)

def summation(n, term):
    if n == 0:
        return 0
    else:
        return term(n)+summation(n-1, term)

def hailstone(n):
    """Print the hailstone sequence starting at n and return its length.

    >>> a = hailstone(10)  # Seven elements are 10, 5, 16, 8, 4, 2, 1
    10
    5
    16
    8
    4
    2
    1
    >>> a
    7
    """
    "*** YOUR CODE HERE ***"
    print(int(n))
    if n == 1:
        return 1
    else:
        if n % 2 == 0:
            n = n / 2
        else:
            n = n * 3 + 1
        return 1 + hailstone(n)
        

def compose1(f, g):
    """"Return a function h, such that h(x) = f(g(x))."""
    def h(x):
        return f(g(x))
    return h

def repeated(f, n):
    """Return the function that computes the nth application of f.

    f -- a function that takes one argument
    n -- a positive integer

    >>> repeated(square, 2)(5)
    625
    >>> repeated(square, 4)(5)
    152587890625
    """
    "*** YOUR CODE HERE ***"
    if n == 1:
        return f
    else:
        return compose1(f,n-1))
