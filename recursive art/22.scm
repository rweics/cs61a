;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: To Kill a Stanfurd Tree.
;;;
;;; Description:
;;;    It's a Stanfurd tree
;;;    Living in a Stanfurd World
;;;    And it can't keep up.


(define(draw)

(define (treeleft length col)
	(begin (color col)
		(cond ((< length 5) 
			(begin (forward length) (back length)))
		(else (begin (forward (/ length 3)) 
					(left 50)
					(treeleft (* length (/ 2 3)) "white")
					(right 50)
					(back (/ length 3))
					(forward (/ length 2))
					(right 25)
					(treeleft (/ length 2) "white")
					(left 25)
					(back (/ length 2))
					(forward (* length (/ 5 6)))
					(right 25)
					(treeleft (/ length 2) "red")
					(left 25)
					(back (* length (/ 5 6)))
					)))))

(define (treeright length col)
	(begin (color col)
		(cond ((< length 5) 
			(begin (forward length) (back length)))
		(else (begin (forward (/ length 3)) 
					(right 50)
					(treeright (* length (/ 2 3)) "yellow")
					(left 50)
					(back (/ length 3))
					
					(forward (/ length 2))
					(left 25)
					(treeright (/ length 2) "yellow")
					(right 25)
					(back (/ length 2))
					
					(forward (* length (/ 5 6)))
					(left 25)
					(treeright (/ length 2) "blue")
					(right 25)
					(back (* length (/ 5 6)))
					)))))
(speed 0)
(color "black")
(penup)
(goto 400 0)
(pendown)
(begin_fill)
(circle 1000)
(end_fill)

(goto -20 -100)
(pendown)
(left 90)
(seth 0)
(treeleft 100 "white")
(penup)
(goto 20 -100)
(pendown)
(treeright 200 "yellow")

(hideturtle)

(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)