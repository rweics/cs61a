;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Inside-out Starfish
;;;
;;; Description:
;;;   Five-pointed wonder 
;;;   Washed upon the gritty sand
;;;   Clinging on to life
(define (starfish distance i) 
((begin 
(color "red") (fd distance)
(fd distance) (color "white") 
(color "black") (rt 108) 
(color "blue") (circle distance)
(fd distance) (color "white") 
(rt 90)
(color "red") (fd distance)
(fd distance) (color "red") 
(fd distance) (color "black") 
(fd distance)
(rt 90)
(fd distance) (color "red") 

(fd distance) (draw (+ distance 1) (+ i 1)) ))
)


(define (draw distance i)
  (if (< i 30)  (starfish distance i) 'done))

(pendown)
(draw 1 1)
(penup)
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)

;(load "contest.scm")