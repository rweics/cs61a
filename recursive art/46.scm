
;;; Scheme Recursive Art Contest Entry
;;;
;;;
;;; Title: SNOWFLAKE
;;;
;;; Description:
;;;   Winter is coming! 
;;;   Christmas and snow are my loves.
;;;   Snowflake will appear.

(define (draw)
(define size 50) ; size should be from 50 to 100 for better look
(define a (/ size 12.5))(define b (/ size 7.1428))(define c (/ size 25)) ;diameter of curves
(define w1 (/ size 20))(define w2 2)    ; thickness in one branch
(define (checkx angle deep)
  (cond ((or (eq? angle 0) (eq? angle 180)) 0)
        ((or (eq? angle 60) (eq? angle 120)) (* -1.73205 size))
        ((or (eq? angle 240) (eq? angle 300)) (* 1.73205 size))
  )
)
(define (checky angle deep)
  (cond ((eq? angle 0)  (* 2 size))
        ((or (eq? angle 60) (eq? angle 300)) size)
        ((or (eq? angle 120) (eq? angle 240)) (- size) )
        (else (* size -2))
  )
)
(define (curves radius)
       (circle radius 270)(pu)(circle radius 90)(pd)
       (circle (- radius) 270)(pu)(circle (- radius) 90)(pd)
)
(define (branch x y angle color)  ; one branch of a snowflake   
  (pu)
  (setpos x y)
  (lt angle)
  (pd)
  (scmode 255)
  (pencolor color 191 255)
  (width w1)
  (fd (/ size 2))
  (width w2)(curves a)
  (fd (* size 0.3))(curves b)
  (width 1)(fd (* 0.2 size))(curves c)
  (setpos x y)
  (seth 0)
)
(define (oneset deep angle x y color)  ;one snowflake
  (if (< angle 360) (begin (branch x y angle color) (oneset deep (+ angle 60) x y color)))
 ) 
(define (create deep angle)  ;recursion in deep and angle
  (if (eq? deep 1)  (oneset deep angle 0 0 25) 
     (if (eq? angle 360) (create (- deep 1) 0) 
        (begin      
          (oneset deep 0 (* (- deep 1) (checkx angle deep)) (* (- deep 1) (checky angle deep)) 
            (if (> deep 5) 250 (+ 25 (* 40 deep))))
          (create deep (+ angle 60))) 
     )
  )
)
(define (reset)
    (lt -90)(bye)  ;deactive this line for other type of snowflake
    (bgcolor "black") ;set background color
    (setpos 0 0)(seth 0)
    (speed 100)   ;set speed
)   
(reset)
(create 5 0) ; Recursion depth is 5
(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)

