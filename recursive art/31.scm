;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Circular Emotion>
;;;
;;; Description:
;;;   <The human spirit 
;;;    A neverending spiral
;;;    Of colors and dreams>

(define (help2 ang color)
	(cond ((= ang 360) )
		(else (help 0 color)
			(rt 45)
			(help2 (+ ang 45) color))))

(define (help size col)
	(cond ((or (> size 100) (< size -100)) ) 
		(else (color col) 
			;(begin_fill)
			(circle size)
			;(end_fill)
		(help (+ size 1) col))))

(define (draw)
	(hideturtle)
	(speed 0)
	(help2 315 "GREEN")
	(help2 315 "RED")
	(help2 315 "GREEN")
	(help2 315 "RED")
	(help2 315 "GREEN")
	(help2 315 "RED")
	(help2 315 "GREEN")
	(help2 315 "RED")
	(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
