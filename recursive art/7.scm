;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Spirals wow
;;;
;;; Description:
;;;   Colors spirals wow recursion
;;;   wow scheme colors wow
      wow
;;;    

(define (draw)
  (define colors (list "red" "blue" "green" "yellow" "orange" "purple" "violet"))
  (define get (lambda (l k) (if (= k 0) (car l) (get (cdr l) (- k 1)))))
  (define (firstpart time jump)
	(cond ((= time 180) nil)
		(else
		(begin 
			(color (get colors (modulo time 7)))
			(begin_fill)
			(end_fill)
			(left time)
			(forward jump)
			(firstpart (+ time 1) jump)
		)
		)
	)
  )
  (speed 0)
  (color "green")
  (circle 20)
  (firstpart 0 30)
  (penup)
  (setpos 100 120)
  (pendown)
  (circle 20)
  (firstpart 10 -23)
  (pu)
  (setpos -200 0)
  (pd)
  (color "blue")
  (circle 20)
  (firstpart 16 28)
  (hideturtle)			
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
