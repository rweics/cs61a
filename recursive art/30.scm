;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Natural Beauty
;;;
;;; Description:
;;;  <A set of phloem
;;;   Colored and in circle form,
;;;   Nature's beautiful.>

(define (draw)
    (define colors (list "red" "blue" "green" "orange" "purple"))
    (speed 0)
    (width 1)
    (pu)
    (fd 200)
    (pd)
    (right 90)
    (define (draw_item position)
        (if (< position 9)
            (begin 
                (draw_element 0 0)
                (pu)
                (fd 75)
                (right 40)
                (fd 75)
                (draw_item (+ position 1)))))
    (define (draw_element element element_color)
        (if (< element 9)
            (begin
                (if (> element_color 4) (define element_color (- element_color 5)))
                (color (get colors element_color))
                (define element_color (+ element_color 2))
                (draw_segment 0)
                (draw_element (+ element 1) element_color))))
    (define (draw_segment segment)
        (if (< segment 80)
            (begin (pd) (fd 5) (left segment) (pu) (draw_segment (+ segment 1)))))
    (define (get list item)
        (if (= item 0)
            (car list)
            (get (cdr list) (- item 1))))
    (draw_item 0)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
