;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Circle of Life
;;;
;;; Description:
;;;   Like our exam grades,
;;;   Life is a big round circle.
;;;   Where does it all end?

(define (list-ref l k) 
   (if (= k 0) 
     (car l) 
     (list-ref (cdr l) (- k 1))))

(define (c r segs n code start-color)
  (cond ((eq? n 360))
        (else (begin (color (list-ref (list "red" "orange" "yellow" "green" "blue" "purple" "violet") start-color))
                     (fd (* segs r))
                     (left segs)
                     (code)
                     (c r segs (+ n segs) code (modulo (+ start-color 1) 7))))))

(define (draw)
  (speed 0)
  (penup)
  (backward 190)
  (right 90)
  (backward 25)
  (pendown)
  (define (circle-code-small) (circle 5))
  (define (circle-code-med) (c 2 5 0 circle-code-small 0))
  (c 3 10 0 circle-code-med 0)
  (exitonclick)
  )

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
