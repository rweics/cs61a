;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Owl Flag
;;;
;;; Description:
;;;    how many tokens
;;;    does it take to get to the
;;;    recursive center?
(define (repeat k fn)
  (if (> k 0)
      (begin (fn) (repeat (- k 1) fn))
      'done))

(define (triangle len fn)
  (repeat 3 (lambda ()
              (fn) (fd len) (lt 120))))

(define (sierpinski len depth)
  (triangle len (lambda()
                  (if (= depth 0)
                    nil
                    (sierpinski (/ len 2) (- depth 1))))))

(define (octaY d k)
  (octY d (lambda ()
           (if (zero? k) nil
             (octaY (* 1 (/ d 3.41421356237)) (- k 1))))))

(define (octY d fn)
  (color "yellow")
  (begin_fill)
  (repeat 8 (lambda ()
              (fn) (fd d) (lt 45)))
  (end_fill)
  (color "black"))

(define (draw)
  (rt 90)
  (speed 0)
  (octay 100 3)
  (penup)
  (fd 241.421356237)
  (pendown)
  (octay 100 3)
  (lt 180)
  (sierpinski 141.421356237 4)
  (fd 341.421356237)
  (rt 90)
  (fd 341.421356237)
  (rt 135)
  (fd 100)
  (lt 45)
  (fd 401.421356237)
  (lt 45)
  (fd 100)
  (rt 135)
  (fd 341.421356237)
  (fd 400)
  (rt 90)
  (fd 25)
  (rt 90)
  (fd 400)
  (lt 90)
  (fd 200)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)