;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Sierpinski Circles>
;;;
;;; Description:
;;;   <Dread Pirate John 
;;;    and everyone together 
;;;    Sierpinski circle>

(define (draw)

(speed 100)
(begin (color "black") 
        (goto -400 350)
    (begin_fill) 
        (goto -400 -350) 
        (goto 400 -350) 
        (goto 400 350) 
        (goto -400 350) 
    (end_fill)) 
(setpos 0 0) 
(color "#FFD700") ; color gold
(define (get-side r) (* r 1.732)) ; calculates the side of each equilateral triangle 
(pu)
(setpos 0 -300)
(pd) 
(circle 300)
(pensize 2)
(define (draw-circles r) 

      (circle (/ r 2)) 
      (left 60) 
      (pu) 
      (fd (get-side r))
      (pd) 
      (left 60)
      (circle (/ r 2))
      (left 60) 
      (pu)
      (fd (get-side r)) 
      (pd) 
      (left 60) 
      (circle (/ r 2))
      (left 60)
      (pu) 
      (fd (get-side r))
      (pd) 
      (left 60))


(define (tri-circle r step) 
      (draw-circles r)
      (cond ((> step 0) 

                (color "white")
                (tri-circle (/ r 2) (- step 1))
                
                (left 60)
                (pu)
                (fd (get-side r))
                (pd)
                (left 60)

                (color "#0000FF") ; color blue
                (tri-circle (/ r 2) (- step 1))

                (left 60) 
                (pu) 
                (fd (get-side r)) 
                (pd) 
                (left 60)

                (color "#C71585") ; color magenta
                (tri-circle (/ r 2) (- step 1))

                (left 60)
                (pu)
                (fd (get-side r))
                (pd)
                (left 60))))

(tri-circle 300 5) 
(pu)
(setpos -80 220)
(pd)
(color "#FFD700")
(write "Aaarg!")
(pu)
(setpos 0 0)
(pd)
(regshape) ; registers the image "papajohn.gif" to the tkinter image dictionary and changes the shape of turtle to be that image
(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
