;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: The Beauty of Recurring Flowers
;;;
;;; Description:
;;;   <It's a work of art
;;;    Flowers bloom recursively
;;;    Everlasting joy!>


(define (repeat k fn) (if (> k 0)
                (begin (fn) (repeat (- k 1) fn))
                nil))

(define (draw_one_curve_right petal_size)
  (repeat 90
            (lambda () (fd petal_size) (rt 1))))

(define (draw_one_curve_left petal_size)
  (repeat 90
            (lambda () (fd petal_size) (lt 1))))

(define (draw_leaf_curve levels petal_size fill_color)
  (color fill_color) (begin_fill)
  (draw_one_curve_right petal_size)
  (rt 270)
  (fd 3)
  (lt 90)
  (draw_one_curve_left petal_size)
  (lt 90)
  (fd 3)
  (lt 90)
  (draw_one_curve_right petal_size)

  (if (= 1 levels)
    () 
    (draw_leaf (- levels 1) (* 0.6 petal_size)))
    (end_fill)
  )

(define pink       "#ff00ff")
(define dark-blue  "#0000ff")
(define light-blue "#33cccc")

(define (draw_leaf levels petal_size)
  (repeat 4
        (lambda () (repeat 2
                            (lambda () (cond ((= 1 levels) (draw_leaf_curve levels petal_size light-blue))
                                             ((= 2 levels) (draw_leaf_curve levels petal_size pink))
                                             ((= 3 levels) (draw_leaf_curve levels petal_size dark-blue)))
                              (rt 90)                            
                              ))
                   (rt 90)
        ))
  )

(define (draw)
  (draw_leaf 3 2)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
