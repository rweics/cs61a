;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Imperfect Symmetry
;;;
;;; Description:
;;;   From droplets of code
;;;   Six ripples in a still pond
;;;   Expanding outward

(define (draw)
  (pendown)
  (speed 0)
  (draw-partitions (list-all-partitions 16 16 nil))
  (exitonclick)
)

(define (draw-partitions x)
  (cond 
    ((null? x) (penup))
    (else
      (draw-partition (car x))
      (right 60)
      (draw-partitions (cdr x))
    )
  )
)

(define (draw-partition x)
  (cond 
    ((= (length x) 1)
    (color "blue")
    (circle (* 10 (car x)))
    )
    (else
      (penup)
      (forward (* 10 (car x)))
      (pendown)
      (draw-partition (cdr x))
    )
  )
)
  
(define (list-all-partitions total max-value partitioned)
  (cond
    ((= total 0) (cons partitioned nil))
    ((< total 0) nil)
    ((= max-value 0) nil)
    (else
      (append
        (list-all-partitions (- total max-value) max-value (cons max-value partitioned))
        (list-all-partitions total (- max-value 1) partitioned)
      )
    )
  )
)

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
