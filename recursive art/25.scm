;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Spiralling Away
;;;
;;; Description:
;;;    Primes will never end
;;;    Try to find the largest prime
;;;    It's impossible.

(define (draw)
  (speed 0)
  (draw-prime-spiral 100)
  (ht)
  (exitonclick))

(define (get-item list k) 
  (if (= k 0) (car list) (get-item (cdr list) (- k 1))))

(define (choose_color x) 
  (cond ((= 0 (modulo x 4)) "blue")
        ((= 1 (modulo x 4)) "#006633")
        ((= 2 (modulo x 4)) "black")
        (else "#660000")))

(define (repeat k fn)
  ; Repeat fn k times.
  (if (> k 1)
      (begin (fn) (repeat (- k 1) fn))
      (fn)))

; makes a solid square
(define (square s)
    (begin_fill) 
    (repeat 4 (lambda () (fd s) (lt 90)))
    (end_fill))

(define (draw-prime-spiral k)
  (cond ((= k 1) 
          (begin (color "#006633") (circle 2 90) (square 4)))
        (else (begin (draw-prime-spiral (- k 1)) 
                (color (choose_color k))
                (circle (get-item primes (- k 1)) 90)
                (square 4)))))

(define primes '(2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 
				53 59 61 67 71 73 79 83 89 97 101 103 107 109 113
				127 131 137 139 149 151 157 163 167 173 179 181
				191 193 197 199 211 223 227 229 233 239 241 251 
				257 263 269 271 277 281 283 293 307 311 313 317 
				331 337 347 349 353 359 367 373 379 383 389 397 
				401 409 419 421 431 433 439 443 449 457 461 463 
				467 479 487 491 499 503 509 521 523 541))
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
