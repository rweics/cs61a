;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: The Denero Tree
;;;
;;; Description:
;;;    The Denero Tree
;;;    Grown From Denero Himself
;;;    The Root Of All Code

(define (draw)
  (penup)
  (back 150)
  (pendown)
  (color "brown")
  (tree 120 10)
  (exitonclick))

; Mostly recursion, Denero just helps me draw my tree :P
(define (tree length w) 
	(shape "denero.gif")
	(width w)
	(cond ((> length 20)
		   (fd length) (rt 30) 
		   (if (> length 40) (tree (- length 20) (- w 2)) nil) 
		   (lt 60) 
		   (if (> length 40) (tree (- length 20) (- w 2)) nil)
		   (if (<= length 40) (tree (- length 20) (- w 2)) nil)
		   (rt 30) (back length))
		  (else nil)))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
