;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Lambda Flower
;;;
;;; Description:
;;;    A lambda flower
;;;    Made out of parentheses
;;;    Recursion depth one

; The glorious repeat function.
(define (repeat x fn)
  (if (> x 0) (begin (fn) (repeat (- x 1) fn))
    nil))

; Draw a parenthesis, indicate direction of turn.    
(define (parenthesis turn size)
  (repeat 30 (lambda () (fd size) (turn 3))))

; Draw the flower stem out of 10 parentheses in series, paralleled twice.
(define (stem)
  (repeat 2 (lambda ()
    (repeat 10 (lambda ()
      (parenthesis rt 1) (lt 90)))
    (pu) (lt 45) (fd 20) (lt 135) (pd))))

; Draw a cool-looking leaf out of three parentheses.
(define (leaf first second size)
  (parenthesis first (* 2 size))
  (rt 180)
  (parenthesis second size) (parenthesis first size))

; Draw the flower base out of six leaf-shapes.
(define (sepals)
  (repeat 3 (lambda ()
    (leaf lt rt 1) (lt 120)))
  (lt 180)
  (repeat 3 (lambda ()
    (leaf rt lt 1) (rt 120))))

; Draw the flower out of eight leaf-shapes.    
(define (petals)
  (repeat 4 (lambda ()
    (leaf rt lt 2.5) (rt 72)))
  (rt 150)
  (repeat 4 (lambda ()
    (leaf lt rt 2.5) (lt 72))))
    
; Tilted to look like a lambda.
(define (draw)
  (bk 200) (lt 75) (clear)
  (color "green") (stem)
  (pu) (goto -120 -40) (lt 30) (pd)
  (color "dark green") (leaf lt rt 3.5)
  (pu) (goto -152 28) (lt 60) (pd)
  (color "light green") (sepals)
  (rt 180)
  (color "magenta") (petals)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
