;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <One Way Ticket to the 3rd Dimension>
;;;
;;; Description:
;;;   <This is a warning:
;;;    No epileptic should view
;;;    This code as it runs.>

(define (draw)
 (begin 
 (func 1 10000 3000 0 0 0 0 0 0 0)

 (func 45 100 50 -350 5 205 10 0 25 0)  ;

 (func 45 100 -50 -350 6 -25 10 0 25 0) ;

 (func 40 100 0 -350 0 180 10 0 25 0)

 (func 40 100 0 -225 1 180 10 0 25 0)
 
 (func 40 100 0 -350 4 0 10 0 25 0)

 (func 40 100 0 -225 2 0 10 0 25 0)

 (func 70 100 0 -500 3 90 5 0 10 0))
 (exitonclick))

(speed 0)

(define (func num n x y c h dn dx dy dh)
    (if (> num 0)
    (begin 
        (cond 
	    ((eq? num 1) (color "#00FFFF"))
            ((eq? (modulo c 7) 6) (color "green"))
            ((eq? (modulo c 7) 5) (color "red"))
            ((eq? (modulo c 7) 4) (color "blue"))
            ((eq? (modulo c 7) 3) (color "orange"))
            ((eq? (modulo c 7) 2) (color "purple"))
            ((eq? (modulo c 7) 1) (color "yellow"))
            ((eq? (modulo c 7) 0) (color "black"))
                )

        (penup)
    (setposition x y)
    (setheading h)
    (pendown)
    (begin_fill)
    (circle n)
    (end_fill) 
    (func (- num 1) (+ dn n) (+ dx x) (+ y dy) (+ c 1) (+ dh h) dn dx dy dh))
    nil))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)

