;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Peach Spiral Blossom
;;;
;;; Description:
;;;   <Radiant Blossom;
;;;    Folding open, for sunlight;
;;;    Catch joy in your beams>

(define (draw)

	; for "grad" method, see scheme_primatives.py file 
	; (increases the gradiant of a color by a certain hexidecimal amount)

  (define (spiral_line x d1 d2 l1 l2 c g i) ;(x = how far) (d1=starting direction) (d2=left or right) (l1=starting length) (l2=scaling length)(c=color) (g=gradient type) (i=gradient intesity)
  	(if (<= x 0) nil
  		(begin
  		(speed 0)
  		(define c (grad c g i))
  		(color c)
  		(setheading (* d2 (+ d1 2)))
  		(forward (+ l1 l2))
  		(pu)(backward (+ l1 l2))(pd)
  		(spiral_line (- x 2) (+ d1 2) d2 (+ l1 l2) l2 c g i)
  		)))
  (define co1 '"#ff0000") ;red
  (define gr11 '"rd")
  (define gr12 '"ru")
  (define co2 '"#00ff00") ;green
  (define co21 '"#00f000")
  (define gr21 '"gd")
  (define gr22 '"gu")
  (define co3 '"#0000ff") ;blue
  (define gr31 '"bd")
  (define gr32 '"bu")

  (define co4 '"#f0f0f0") ;even colors
  (define co5 '"#101010") ;darker even colors
  (define co6 '"#c000c0") ;purple-ish

  (list co1 co2 co3 co4 co5 co6)

;  (spiral_line 180 0 1 5 co1 gr11 '"01")
;  (spiral_line 180 0 -1 5 co2 gr21 '"01")
;  (spiral_line 180 180 1 5 co3 gr31 '"01")
  (pu)(setpos -45 10)(pd)
  (spiral_line 180 120 1 20 2 co1 gr11 '"01")
  (pu)(setpos 45 10)(pd)
  (spiral_line 180 120 -1 20 2 co1 gr11 '"01")
  (pu)(setpos 0 0)(pd)
  (spiral_line 180 180 -1 20 3 co4 gr21 '"01")
  (spiral_line 180 180 1 20 3 co4 gr21 '"01")
  (pu)(setpos 70 0)(pd)
  (spiral_line 270 0 -1 10 1.5 co2 gr21 '"01")
  (pu)(setpos -70 0)(pd)
  (spiral_line 270 0 1 10 1.5 co2 gr21 '"01")
 ; (pu)(setpos 0 100)(pd)
  ;(spiral_line 306 180 1 5 co5 gr12 '"01")
  ;(pu)(setpos 0 -100)(pd)
  ;(spiral_line 180 0 1 5 co5 gr32 '"01")

  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
