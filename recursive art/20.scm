;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Around the Plane in 10000 Steps
;;;
;;; Description:
;;;   The path goes ever
;;;   on and on, a random walk
;;;   from where it began


; glibc rand(), more or less, divided by the largest possible number
(define (randint seed)
  (modulo (+ (* seed 1103515245) 12345) 2147483648))

(define (randint->random x)
  (/ x 2147483648))

; Shamelessly ripped from the Python standard library
(define (normalvariate seed)
  (let ((r1 (randint seed)))
    (let ((rnd*2pi (* (randint->random r1) 6.283185307179586))
	  (g2rad (sqrt (* -2.0 (log (- 1.0 (randint->random (randint r1))))))))
      (list (* 2 (cos rnd*2pi) g2rad)
	    (* 2 (sin rnd*2pi) g2rad)
	    (randint r1)))))

(define (color-for n)
  ; Gradient from #010066 to #FFCC33 (where n goes from 10000 to 0)
  (list (+ 255 (* (/ (- 1 255) 10000) n))
	(+ 204 (* (/ (- 0 204) 10000) n))
	(+ 51 (* (/ (- 102 51) 10000) n))))

(define (walk n seed)
  (if (not (= n 0))
      (let ((vals (normalvariate seed))
	    (color (color-for n)))
	(color3 (car color) (cadr color) (caddr color))
	; 57.29577951308232: number of radians in one degree
	(seth (* (atan2 (cadr vals) (car vals)) 57.29577951308232))
	(fd (hypot (car vals) (cadr vals)))
	(walk (- n 1) (caddr vals)))))

(define (arrow)
  (begin_fill)
  (lt 150)
  (fd 10)
  (lt 120)
  (fd 10)
  (lt 120)
  (fd 10)
  (end_fill))

(define (x-axis)
  (pu)
  (pensize 3)
  (goto -300 0)
  (pd)
  (seth 90)
  (fd 600)
  (arrow))

(define (y-axis)
  (pu)
  (pensize 3)
  (goto 0 -300)
  (pd)
  (seth 0)
  (fd 600)
  (arrow))

(define (draw)
  (speed 0)
  (x-axis)
  (y-axis)
  (pu)
  (pensize 1)
  (goto 0 0)
  (pd)
  (walk 10000 4061043148) ; because 4061043148 is such a great seed
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
