;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Much Color. So Rainbow. Wow.
;;;
;;; Description:
;;; My eyes are flashin'
;;; from all the vibrant colors
;;; YOLOSWAG RAINBOW.

(define (draw)
  (bgcolor 'black)
  (speed 0)
  (move 1 1000))

(define (move accum limit)
  (cond ((< accum limit)
         (modulatecolor accum)
         (fd (+ (* 0.75 accum)
                (* 0.00000075 (* accum (* accum accum)))))
         (rt 91)
         (define accum (+ 1 accum))
         (move accum limit))
        (else (exitonclick))))

(hideturtle)
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
