;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Next Year>
;;;
;;; Description:
;;;   <One and eleven,
;;;    We need a new quarterback.
;;;    I vote DeNero.>

(define (draw)
(define (choo x)
  (cond
    ((= x 1) (color "#e5c100"))
    ((= x 2) (color "blue"))
    (else (choo (- x 2)))
    ))
(define (repeat k fn co)
  (if (> k 1)
      (begin (choo co) (fn) (repeat (- k 1) fn (+ co 1)))
      (fn)))
(define (poly sides d)
  (if (> sides 2)
  (repeat sides (lambda () (fd d) (lt (- 180 (/ (* (- sides 2) 180) sides))
    ))1)))
(repeat 60 (lambda () (begin (poly 30 40) (rt 6)))
  1)
(ht)
(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
