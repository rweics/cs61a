;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Hexagonal Rose
;;;
;;; Description:
;;;   <Hexagons are cool
;;;    Three attached makes a Combee
;;;    Here is a flower

(define (draw)
  (speed 0)
  (ht)
  (color "purple")
  (hexagons 100 4)
  (color "red")
  (lt 20)
  (hexagons 90 4)
  (lt 20)
  (color "blue")
  (hexagons 80 4)
  (lt 20)
  (color "orange")
  (hexagons 100 4)
  (lt 20)
  (color "green")
  (hexagons 90 4)
  (lt 20)
  (color "pink")
  (hexagons 80 4)
  (lt 20)
  (color "turquoise")
  (hexagons 100 4)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.

(define (hexagons d k)
  (st)
  (pd)
  (if (eq? k 0) (draw-clockwise d 6 0)
      (begin (draw-three d) (hexagons (/ d 2) (- k 1)) )))

(define (draw-clockwise d side start)
  (if (= side start) (pd)
    (begin (fd d) (lt 300) (draw-clockwise d side (+ 1 start)))))

(define (draw-counter d side start)
  (if (= side start) (pd)
     (begin (fd d) (lt 60) (draw-counter d side (+ 1 start)))))

(define (draw-three d)
  (draw-clockwise d 6 0)
  (draw-counter d 6 0)
  (lt 120)
  (draw-clockwise d 6 0)
  (draw-counter d 6 0))

(draw)
