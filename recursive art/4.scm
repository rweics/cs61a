;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Gears
;;;
;;; Description:
;;; Industry Revolution speeds up everything
;;; But don't hurry, don't follow the fashion
;;; Walk your life as slow as the gears

(define (circ r) 
    (pu) (fd r) (lt 90) 
    (pd) (circle r) (pu)
    (rt 90) (bk r))
(define (resetc) (color "#48D1CC") (pendown))
(define (getitem lst index)
    (if (= index 0) (car lst) (getitem (cdr lst) (- index 1))))
(define radius '(100 80 70 60 50 40 30 20))
(define pos '(19 80 70 50 10 40 20 30))
(define (gear r chance)
    (define shape (* r 0.4))
    (if (< shape 10) (define shape 10))
    (define angle (/ 360 shape))
    (define base-angle 75)
    (define top (+ 0.3 (* r 0.006)))
    (begin_fill)
    (define (teeth shape)
            (if (eq? shape 0)
                (begin
                    (penup) (rt (- 90 (/ angle 2))) (fd r)
                    (circ (* 0.5 r))
                    (cond 
                        ((< chance 4) (color "#4F4D4D"))
                        ((> chance 7) (color "#D9D6CF"))
                        (else (color "#A2A3A2")))
                    (end_fill) (fd (getitem pos chance)) (resetc)
                )
                (begin
                    (define base-angle 75)
                    (define length (* r (sin (/ angle 2))))
                    (define c (/ 0.5 (cos 75)))
                    (lt base-angle) (fd (* c length))
                    (rt base-angle) (fd (* top length))
                    (rt base-angle) (fd (* c length))
                    (lt base-angle) (fd (* (- 1 top) length))
                    (rt angle)
                    (teeth (- shape 1))
                )
            )
    )
    (teeth shape)
    (if (< chance 8) (gear (getitem radius chance) (+ 1 chance)))
)

(define (draw)
    (reset)
    (speed 0)
    (screensize 400 300 "#171916")
    (resetc)
    (gear (getitem radius 0) 1)
    (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)