;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <The Dread Pirate Lambda's Fibonautical Flag Computes fib(7)>
;;;
;;; Description:
;;;   <Eight-sided eye, for
;;;    fib(7) is eight; in teeth,
;;;    fib(6) plus fib(5)>
;;;
;;; The outermost skull has an octagon inside it because fib(7) = 8.
;;; There are two more skulls in the outer skull's teeth. These skulls represent the computation
;;; of fib(7) as fib(7) = fib(6) + fib(5), so these two skulls have a pentagon (fib(6) = 5) and a
;;; triangle (fib(5) = 3), respectively.

(define (draw)
  ; *YOUR CODE HERE*
  (bgcolor "black")
  (goto 0 -176)
  (rt 90)
  (draw-fib-skull 6 375)
  (goto 0 -196)
  (setheading 75)
  (color "white")
  (fill-full-crossbone)
  (goto 0 -196)
  (setheading 105)
  (fill-full-crossbone)
  (ht)
  (exitonclick))

(define (fill-full-crossbone)
  (begin_fill)
  (draw-half-crossbone)
  (draw-half-crossbone)
  (end_fill))
  
(define (draw-half-crossbone)
  (exec-forward-then-backward '(
  (fd 280)
  (lt 58) ; a
  (circle -23 180)
  (lt 64)))) ; 180 - a - a
  
(define draw-eye (mu ()
  (pendown)
  (begin_fill)
  (circle eye-rad 360)
  (end_fill)
  (penup)))
  
; num is the INDEX of the fibonacci number
; So num=0 corresponds to 0, num=1 corresponds to 1, num=2 corresponds to 1, num=3 corresponds to 2, and so on
(define (draw-fib-skull num height)
  (let ((has-small-skulls (> num 1))
        (mouth-rad (* height 0.5))
        (eye-rad (* height 0.12))
        (sum num))
  (color "white")
  (begin_fill)
  (draw-skull-outline mouth-rad)
  (end_fill)
  (left-turn)
  (fd (* mouth-rad 1.2))
  (left-turn)
  (fd mouth-rad)
  (lt 175)
  (color "black")
  (begin_fill)
  (circle (/ mouth-rad (sin (radians 5))) 10)
  (lt 85)
  (circle mouth-rad -180)
  (end_fill)
  (circle mouth-rad 48)
  (if has-small-skulls (define sum (draw-fib-skull (- num 1) (* height 0.27))))
  (circle mouth-rad 84)
  (if has-small-skulls (define sum (+ sum (draw-fib-skull (- num 2) (* height 0.27)))))
  (circle mouth-rad 48)
  (penup)
  (fd mouth-rad)
  (left-turn)
  (fd (* mouth-rad 0.5))
  (draw-eye)
  (color "cyan")
  (pensize (+ 1 (* eye-rad 0.06)))
  (pendown)
  (cond ((< sum 2) (polygon eye-rad (+ 1 sum)))
        ((= sum 2) (exec-forward-then-backward '((penup) (fd (/ eye-rad 2)) (begin (pendown) (polygon eye-rad 2) (penup)) (bk eye-rad))))
        (else (polygon eye-rad sum)))
  (penup)
  (pensize 1)
  (color "black")
  (fd mouth-rad)
  (draw-eye)
  (bk (* mouth-rad 0.5))
  (left-turn)
  (fd (* mouth-rad 2.2))
  (left-turn)
  (pendown)
  sum))
  
; Procedure that draws the outline of a skull.
; Precondition: The turtle is oriented tangent to the chin of the skull face, in the direction of the skull's left eye.
; Postcondition: The turtle is in the same position as it was when the skull is drawn.
(define (draw-skull-outline half-height)
    (exec-forward-then-backward '(
       (circle half-height 90)
       (rt 45)
       (circle (* half-height 1.1) 45)
       (circle (* half-height 0.9) 70)
       (circle (* 2.1343791701554813 half-height) 40))))
       ; 2.1343791701554813 = (/ (- 1 (- (* 0.9 (- 1 (sin (radians 20)))) (* 1.1 (- 1 (/ 1 (sqrt 2)))))) (sin (radians 20)))

(define (left-turn) (lt 90))

; executes a list of instructions forward and then backward, executing the last instruction only once
; ex. if you pass in the list (exp1 exp2 exp3) this procedure will evaluate exp1, then exp2, then exp3, then exp2, then exp1
(define exec-forward-then-backward (mu (commands)
  (eval (car commands))
  (cond ((not (null? (cdr commands))) 
            (exec-forward-then-backward (cdr commands))
            (eval (car commands))))))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
