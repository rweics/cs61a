;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Flower Abyss 
;;;
;;; Description:
;;;     Flowery Abyss;
;;;   	Exploring great depths just for-
;;;    	Three Extra Credits.

(define (repeat fn k)
    (if (> k 1)
        (begin (fn) (repeat fn (- k 1)))
        (fn)))

(define (draw-leaf s)
    (circle (* s 10) 90)
    (lt 90)
    (circle (* s 10) 90))

(define (draw-fan s)
    (repeat (lambda () (draw-leaf s) (lt 30)) 6))


(define (draw-branch-1 s d)
    (if (> d 1)
        (begin (draw-branch-2 s 5) (penup) (circle (* s 31) 30) (pendown) (draw-branch-1 (* s 1.15) (- d 1)))
        (draw-branch-2 s 5)))

(define (draw-branch-2 s d)
    (if (> d 1)
        (begin (draw-fan s) (draw-branch-2 (* s 1.1) (- d 1)))
        (begin (draw-fan s))
    ))

(define (draw)
  	(speed 0)
  	(draw-branch-1 2 25)
  	(hideturtle)
  	(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
