;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Midnight Flower
;;;
;;; Description:
;;;   <Out of the darkness
;;;    come the beautiful colors
;;;    to bedazzle us.>

(define (draw)
  (speed 10)
(define (call-color bc)
  (cond ((= bc 1) (color "red"))
        ((= bc 2) (color "orange"))
        ((= bc 3) (color "yellow"))
        ((= bc 4) (color "lightgreen"))
        ((= bc 5) (color "pink"))
        ((= bc 6) (color "darkgreen"))
        ((= bc 7) (color "blue"))
        ((= bc 8) (color "purple"))
        ((= bc 9) (color "violet"))
        ((= bc 10) (color "pink"))
        ((= bc 11) (color "red"))
        ((= bc 12) (color "orange"))
        ((= bc 13) (color "yellow"))
        ((= bc 14) (color "coral"))
        ((= bc 15) (color "green"))
        ((= bc 16) (color "red"))
        ((= bc 17) (color "orange"))
        ((= bc 18) (color "yellow"))
        ((= bc 19) (color "lightgreen"))
        ((= bc 20) (color "pink"))
        ((= bc 21) (color "darkgreen"))
        ((= bc 22) (color "blue"))
        ((= bc 23) (color "purple"))
        ((= bc 24) (color "violet"))
        ((= bc 25) (color "pink"))
        ((= bc 26) (color "red"))
        ((= bc 27) (color "orange"))
        ((= bc 28) (color "yellow"))
        ((= bc 29) (color "coral"))
        ((= bc 30) (color "green"))
        (else (color "darkgreen"))))


(define (draw-circle size angle bc k)
(if (= bc 0) ('0)
  ((call-color bc) 
    (begin_fill)
    (circle size) (right angle)(end_fill)
    (draw-circle (* k size) angle (- bc 1) k))))

(goto 500 0)
(color "black")
(begin_fill)
(circle 1250)
(end_fill)

(goto 0 0)

(draw-circle 100 15 24 1)

(define (outline-circle size angle bc k)
(if (= bc 0) ('0)
  ((color "white")
    (circle size) (right angle)
    (outline-circle (* k size) angle (- bc 1) k))))

(outline-circle 100 15 24 1)

  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)