;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Meaning through Eternity
;;;
;;; Description:
;;;   Forgotten Base case?
;;;   How can we end our Madness?
;;;   But why should we stop?

;;; draws person
(define (drawperson x y xlen ylen)
	(penup) (setposition (+ x (/ xlen 2)) y)(setheading -90) (pendown) (pensize (/ xlen 20)) (circle (/ ylen 8) 360) (penup)
	(setposition (+ x (/ xlen 2)) (- y (/ ylen 4))) (setheading 180) (pendown) (forward (/ ylen 2)) (penup)
	(right 45) (pendown) (forward (* 1.4142135623 (/ xlen 2))) (penup)
	(setposition (+ x (/ xlen 2)) (- y (* ylen .75))) (setheading 135) (pendown) (forward (* 1.4142135623 (/ xlen 2))) (penup)
	(setposition x (- y (/ ylen 3))) (setheading 90) (pendown) (forward xlen) (penup))

;;; draws person spawning persons spawning more persons spawning more persons spawning more persons spawning more persons spawning more persons spawning more persons spawni
(define (peopletree x y xlen ylen)
	(cond ((<= xlen 1) (hideturtle))
		(else (drawperson x y xlen ylen)
			(peopletree x (- y ylen) (/ xlen 2) (/ ylen 2))
			(peopletree (+ x (/ xlen 2)) (- y ylen) (/ xlen 2) (/ ylen 2)))))

(define (draw)
  (speed 0) (peopletree -100 410 200 400)(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
