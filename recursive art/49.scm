;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: What did John Say?
;;;
;;; Description:
;;;    If you don't give up,
;;;    This class won't give up on you.
;;;    Have a lollipop!

(define (draw)
  ; *YOUR CODE HERE*
  (background)

  (fox)
  (pu)
  (goto -87 75)
  (pd)
  (tree 190)

  (exitonclick))

(define (background)
	(bgcolor 'black))

(define (fox)
	(color "orange")
	(begin_fill)
	(lt 90)
	(fd 25)
	(rt 90)
	(fd 60)
	(rt 30)
	(fd 20)
	(rt 120)
	(fd 20)
	(lt 60)
	(fd 40)
	(lt 60)
	(fd 20)
	(rt 120)
	(fd 20)
	(rt 30)
	(fd 60)
	(rt 90)
	(fd 25)
	(lt 90)

	(fd 5)
	(lt 90)
	(fd 10) (rt 90) (fd 10) (rt 90) (fd 10) (lt 90)
	(fd 10)
	(fd 10) (rt 90) (fd 10) (rt 90) (fd 10) (lt 90)
	(fd 10)
	(lt 90)
	(fd 10) (rt 90) (fd 10) (rt 90) (fd 10) (lt 90)
	(rt 90)
	(fd 10)
	(lt 90)
	(fd 10) (rt 90) (fd 10) (rt 90) (fd 10) (lt 90)
	(fd 10)
	(end_fill)


	(pu)
	(goto 40 35)
	(pd)
	(color "brown")
	(begin_fill)
	(circle 10)
	(end_fill)
	(pu)
	(goto 10 35)
	(pd)
	(color "brown")
	(begin_fill)
	(circle 10)
	(end_fill)
	
	(color "white")
	(begin_fill)
	(pu)
	(goto 0 20)
	(pd)
	(rt 90)
	(fd 30)
	(rt 120)
	(fd 10)
	(rt 120)
	(fd 10)
	(lt 120)
	(fd 10)
	(rt 120)
	(fd 10)
	(lt 120)
	(fd 10)
	(rt 120)
    (fd 10)
    (end_fill)

    (pu)
    (goto -9 -12)
    (pd)
    (color "green")
    (begin_fill)
    (rt 3)
    (fd 115)
    (lt 90)
    (fd 10)
    (lt 90)
    (fd 115)
    (lt 90)
    (fd 10)
    (end_fill))

(define (tree n)
	(if (> n 0)
		((cond 
			((> n 175) (color "red"))
			((> n 150) (color "orange"))
			((> n 125) (color "yellow"))
			((> n 100) (color "green"))
			((> n 75) (color "blue"))
			((> n 50) (color "purple"))
			(else nil))
		(fd (/ n 3))
		(lt 30)
		(tree (- n 1))
		(pu))))

(hideturtle)
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
