;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Rose
;;;
;;; Description:
;;;   Hexagons spiral
;;;   In luminescent colors
;;;   A rose is formed.

(define (draw)
  (bgcolor "black")
  (speed 0)
  (pd)
  (define (paths n)
          (rgb (+ (/ (modulo n 6) 12) (* 0.08 (modulo n 7)))
               (+ (/ (modulo n 3)  6) (* 0.08 (modulo n 7)))
               (+ (/ (modulo n 2)  4) (* 0.08 (modulo n 7))))
          (fd n)
          (rt 61)
          (if (< n 840) (paths (+ n 1))))
  (paths 1)
  (exitonclick))
(draw)