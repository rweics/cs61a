;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: 'Dark' Golden (Haiku) Bear
;;;
;;; Description:
;;;   <This bear haunts the dreams
;;;    of those who oppose Berkeley.
;;;    *looks at you Stanford*>

; The Gummy Bear Swarm
(pu) (goto -177 150) (pd)

(define (bear size)
	(color "gold")
	; Right side 
	(circle size) (fd (* 5 size)) 
    (circle size) (fd (* 5 size))
    ; Top ears
    (circle size 180) (lt 90) 
    (pu) (rt 180) (pd) (fd (* size 2)) 
    (rt 90) (circle size 180)
    ; Left side
    (fd (* 5 size)) (circle size) 
    (fd (* 5 size)) (circle size 540) 
    ; Bottom side
    (rt 90) (fd (* size 2))
    ; Eyes and 'lips'
    (pu) (fd (/ size 2)) (lt 90) (fd 90) 
    (pd) (color "blue") (circle (- size 6) 540) (rt 90) 
    (pu) (fd (+ size 6)) 
    (pd) (rt 90) (circle (- size 6) 360)
    (pu) (rt 180) (fd size) 
    (pd) (lt 90) (fd (+ size 6))
    ; Teeth
    (color "red")
    (rt 120) (fd (- size 2)) (rt 120) (fd (- size 2))
    (lt 120) (fd (- size 2)) (rt 120) (fd (- size 2))
    ; Reposition
    (pu) (lt 150) (fd (* size 8)) 
         (lt 90) (fd (* size 4)) (lt 90) (pd))

(define (repeat k object)
	(if (> k 0)
		(begin 
			(object 10)
			; Spacing
        	(pu) (rt 90) (fd 100) (lt 90) (pd)
			(repeat (- k 1) object))
	    'end ))

(define (draw)
  ; *YOUR CODE HERE*
  (speed 0)
  ; Begin the first line of haiku - 5 bears
  (repeat 5 bear)
  ; Next line - 7 bears
  (pu) (goto -278 0) (pd) (repeat 7 bear)
  ; Final line - 5 bears
  (pu) (goto -177 -150) (pd) (repeat 5 bear)

  (exitonclick))
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
