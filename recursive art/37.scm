;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: i think this is called art
;;;
;;; Description:
;;;   look closely towards
;;;	  the center, you will find yourself
;;;	  lol nah just kidding

(speed 0)
(begin_fill)
(color "black")
(forward 1000)
(left 90)
(circle 1000)
(end_fill)
(setpos 0 0)
(define (draw radius times total_circles)
  		(color "white")
  		(cond ((eq? total_circles 0))
  			  ((eq? times 0)
  						(right 10)(circle radius)(forward 10)
  						(draw 200 20 (- total_circles 1)))
  			  ((< times 10)
  			  			(right 10)(circle radius)(forward 10)
        	  			(draw (+ radius 10) (- times 1) total_circles))
        	  (else  	
        	  			(right 10)(circle radius)(forward 10)
        	  			(draw (- radius 10) (- times 1) total_circles))))
 
 (draw 200 20 14)
 (exitonclick))
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
