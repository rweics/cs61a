;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Directional Dragons
;;;
;;; Description:
;;;   All these triangles
;;;   I dare you to calculate
;;;   total area

(define (draw-triangle px py d dir) ;Draws right isoceles triangle with right angle at (px, py), base d, and normal vector to base pointing in dir.
    (begin (penup) (goto px py) (seth (- dir 45)) (pendown) (begin_fill)
         (forward (/ d (sqrt 2))) (right 135)
         (forward d) (right 135)
         (forward (/ d (sqrt 2))) (end_fill)))

(define (levy-dragon depth px py d dir) ;Draws Levy Dragon with center at (px, py) and original length d (opens toward dir).
  (if (= depth 0)
      (begin (color (levy-dragon-color px py)) (draw-triangle px py d dir))
      (begin
        (levy-dragon (- depth 1) (+ px (* (/ d 2) (sin (radians (- dir 90))))) (+ py (* (/ d 2) (cos (radians (- dir 90))))) (/ d (sqrt 2)) (+ dir 45))
        (levy-dragon (- depth 1) (- px (* (/ d 2) (sin (radians (- dir 90))))) (- py (* (/ d 2) (cos (radians (- dir 90))))) (/ d (sqrt 2)) (- dir 45)))))
;Levy Dragon dimensions: 2d x 1.25d

(define (hex-string x) ;Converts decimal x to hexadecimal value and creates two-digit string (x must be between 0 and 255, inclusive).
  (cond ((< x 16) (append-string "0" (to-string (to-hex x))))
        (else (to-string (to-hex x)))))

(define (make-color r g b) ;Returns color based on values of RGB (RGB values must be between 0 and 255, inclusive).
  (append-string "#" (hex-string r) (hex-string g) (hex-string b)))

(define (radial-color angle) ;Returns color based on angle from (0, 0). 
  (define theta (modulo angle 360))
  (cond ((<= theta 120) (make-color (floor (* 255 (/ (- 120 theta) 120))) 0 (floor (* 255 (/ theta 120)))))
        ((and (> theta 120) (<= theta 240)) (make-color 0 (floor (* 255 (/ (- theta 120) 120))) (floor (* 255 (/ (- 240 theta) 120)))))
        (else (make-color (floor (* 255 (/ (- theta 240) 120))) (floor (* 255 (/ (- 360 theta) 120))) 0))))

(define (levy-dragon-color px py) ;Returns color based on point (point (px, py) on each right triangle).
  (radial-color (degrees (arctan (/ py px)))))

(define (color-background) ;Colors background black
  (begin (penup) (goto -400 340) (seth 90) (color "black") (pendown) (begin_fill)
         (forward 800) (right 90)
         (forward 680) (right 90)
         (forward 800) (right 90)
         (forward 680) (end_fill)))

(define (draw) ;Creates four Levy Dragons, each facing different direction
  (begin (speed 0)
         (color-background)
         (levy-dragon 10 0 0 300 0)
         (levy-dragon 10 0 0 300 90)
         (levy-dragon 10 0 0 300 180)
         (levy-dragon 10 0 0 300 270)
         (hideturtle)
         (exitonclick)))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
