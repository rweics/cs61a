;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <ComputerStars>
;;;
;;; Description:
;;;   <Can you see the pun?
;;;    These Blue / gold stars are Berkeley's-
;;;    What an edu!>
;;;    

(define (draw)
  (speed 0)
  (bgcolor '"black")
  (newsetpos 140 0)
  (seth 30)
  (spiral 60 12) 
  (newsetpos 500 100) 
  (seth 40)
  (draw-s 30)
  (ht)  
  (newsetpos -400 -200)  
  (3d-star 120 6 #t #f)
  (newsetpos -150 -250)
  (color '"#344461")
  (begin_fill)
  (circle 25)
  (end_fill)
  (exitonclick))
(define (spiral d k)
    (if (> k 0)
      (begin
        (3d-star d 5 #f #t)
        (left 110)
        (forward (/ d 9))
        (right 90)
        (spiral (* d 0.95) (- k 1)))))

(define (draw-s d)
  (seth 20)
  (spiral d 9)
  (seth 220)
  (newsetpos 390 -130)  
  (spiral d 9))

(define (3d-star d k rays? color?)
  (if (> k 0)
    (begin
  (left 90)
  (forward 3)
   (right 90)
   (if (<= k 3)
    (make-star d rays? (heading) 6 'in color?)
    (make-star d #f (heading) 6 'in color?))
  (3d-star d (- k 1) rays? color?))))

(define (make-star d rays? orig-heading k dir color?)
  (let ((h (heading))) 
    (if (> k 0)
      (begin

        (if (and color? (or (and (= k 1) (eq? dir 'out)) (and (= k 6) (eq? dir 'in))))
          (color '"#344461")
          (color '"#b78727"))
       (forward d)
       (if (and rays? (eq? dir 'out))
          (begin
            (seth (+ orig-heading 245))
          (forward (* 4 d))
          (backward (* 4 d))
          (seth h)))
      (if (eq? dir 'out)
        (begin
          (left 120)
          (make-star d rays? orig-heading (- k 1) 'in color?))
        (begin
          (right 60)
          (make-star d rays? orig-heading k 'out color?)))))))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
