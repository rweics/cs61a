;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: To understand recursion, you must first understand recursion.
;;;
;;; Description:
;;;   Haikus are random.
;;;   I write good code, bad haikus.
;;;   Don't count syllables.

(define (offset_circle s)
        (goto s 0)
        (begin_fill)
        (circle s)
        (end_fill)
        (pu)
        (goto 0 0)
        (pd))

(define (piece size angle)
        (circle size)
        (rt angle))

(define (pattern size n)
        (begin_fill)
        (repeat n piece size (/ 360 n))
        (end_fill))

(define (repeat n fn size angle)
	    (if (> n 0)
	    	(begin (fn size angle)
	        	   (repeat (- n 1) fn size angle))))

(define (draw)
        (speed 0)
        (color "#ffd700")
        (offset_circle 450)
        (color "#0f4d92")
        (pattern 225 250)
        (pattern 225 50)
        (pattern 225 10)
        (hideturtle)
		(exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
