;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Your title here>
;;;
;;; Description:
;;; <"The Fabric of the Cosmos" -- The artistic masterpiece is made by recursively drawing incomplete circles. It is a representation of one of three possible shapes of the universe: spherical. 
;;;   Certain theories of the universe predict that edges of space actually curl upon themselves, resulting in a universe with no edge. 
;;;   This post-modernistic, non-Euclidean drawing captures the power of the human intellect and redefines the limits of human imagination. It is a timeless art piece that will undoubtedly continue to fascinate future civilizations.>

   ; *YOUR CODE HERE*

(define (draw i)
  (if (< i 65) (cir (+ 2 i)) 'done))

(define (cir i) 
	( (lt 11) (circle 50) (draw i)))

(speed 2)
(draw 0)

  
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.



