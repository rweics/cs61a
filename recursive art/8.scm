;;; Title: No title
;;;
;;; Description:

(define (repeat k fn) 
	(if (> k 0)
        (begin (fn) 
        	(repeat (- k 1) fn)
        )
             nil
    )
)


(define (ornament size)
	(if (> size 3) 
			(repeat size (lambda () (fd size) (rt (/ 360 size)))))
	)

(define (add_star size)
	(repeat 5 (lambda () (fd size) (rt 144) (fd size) (lt (+ size 42)))))
	


(define (f x)
	(if (> x 2)
		(begin 
			(color "green")
			(repeat x 
				(lambda () 
					(begin
						(forward (* 6 x)) 
						(right (/ 360 x))
						(if (= (modulo x 2) 0) 
							(begin
								(color "red")
								(ornament (/ x 2))
							)
						(color "green")
						)

					)
				)
			)
			(pu)
			(left 90)
			(forward (* 3 x)) ;always half of the forward distance used to make each shape
			(right 90)
			(pd)	
			(f (- x 1))	
		)
	(begin
		(color "yellow")
		(pu)
		(forward 8)
		(pd)
		(add_star 10)
	)
	)
)
(speed 0)
(f 15)
(pu)
(forward 100)






