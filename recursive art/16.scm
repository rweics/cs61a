;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Spiral
;;;
;;; Description:
;;;   Spirals are so curly
;;;   Spiral spiraly spiral
;;;   Refrigerator

(define (draw)
  (speed 0)
  (fill-background "#073642")
  (pendown)
  (spiral 1000 0 1 45)
  (penup)
  (exitonclick))

;;; Draw a spiral thing
(define (spiral limit init increment angle)
  (cond ((> limit init)
  	       (change-color init)
           (circle init angle)
           (spiral limit (+ init increment) increment angle))))

;;; Fill the background with a certain color
;;; Simply creates and fills in a 1000x1000px square
(define (fill-background color)
	(setpos -500 -500)
	(begin_fill)
	(define (side x) (fd x) (rt 90))
	(repeat 4 (lambda () (side 1000)))
	(end_fill)
	(setpos 0 0))

;;; Change the color based on an input number
;;; Check the single digit factors of n to change the color
;;; Colors come from Ethan Schoonover's Solarized color schemes
(define (change-color n)
  (cond ((= 0 (modulo n 9)) (color "#657b83"))
  	    ((= 0 (modulo n 8)) (color "#859900"))
  	    ((= 0 (modulo n 7)) (color "#2aa198"))
  	    ((= 0 (modulo n 6)) (color "#268bd2"))
  	    ((= 0 (modulo n 5)) (color "#6c71c4"))
  	    ((= 0 (modulo n 4)) (color "#d33682"))
  	    ((= 0 (modulo n 3)) (color "#dc322f"))
  	    ((= 0 (modulo n 2)) (color "#cb4b16"))
  	    (else               (color "#b58900"))))

;;; Function to repeat a function
(define (repeat k fn)
 (if (> k 0)
        (begin (fn) (repeat (- k 1) fn))
        nil))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
