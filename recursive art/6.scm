;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Infinity
;;;
;;; Description:
;;;   A query for you.
;;;   A set contains all sets. Does
;;;   it contain itself?

; generates a list of lists to be fed to the infinity-drawer function
(define (draw)
  ; *YOUR CODE HERE*
  (penup)
  (define inputs (gen-input-lst (list 320 0) 5))
  (hideturtle)
  (infinity-drawer inputs)
)
; cycles through each entry (a well formed scheme list) of the input list, and uses it to draww a single
; infinity symbol
(define (infinity-drawer input-lst)
	(if (null? input-lst)
		nil
	)
	(draw-infinity (car input-lst) (cadr input-lst) 0)
	(penup)
	(infinity-drawer (cdr (cdr input-lst)))
)

; draws a single line for the infinity symbol based on the size shift direction and angle given
(define (draw-infinity size shift theta)
	(cond
		((= theta 361)) ; once 361 has been reached the program has draw the whole infinity symbol
		(else 
			(pensize (* .08 size))
			(if (or (= 45 theta) (= theta 225))
				(define theta (+ theta 90))
			)
		(define r (infinity-r size theta))
		(define xy (cartesian r theta))
		(goto (+ (car xy) shift) (cadr xy))
		(pendown)
		(draw-infinity size shift (+ theta 1))
		)
	)
)
; takes in a list of the size shift and direction of an infinity and returns a list containing the size,
; shift, and direction for each of the infinities inside the current
(define (gen-input-lst starters limit)
	(cond 
		((= 0 limit) starters)
		(else		
			(flatten (list 
						starters 
						(gen-input-lst (list (/ (car starters) 2)
								    		 (+ (cadr starters) (* .5 (sqrt 2) (car starters))) 
					 	   	  		   ) 
									   (- limit 1)
						)
					 	
					 	(gen-input-lst (list (* .5 (car starters))
							       			 (- (cadr starters) (* .5 (sqrt 2) (car starters)))
							       	   )
							       	   (- limit 1)
					 	)
					 )
			)
		)
	)
)

(define (flatten list)
   (cond 
   		((null? list) nil)
        ((list? (car list)) (append (flatten (car list)) (flatten (cdr list))))
        (else (cons (car list) (flatten (cdr list))))
   )
)

; generates the proper radius for the shape base on angle size and direction
(define (infinity-r size theta)
	(cond
		((or (and (<= 45 theta) (>= 135 theta)) (and (<= 225 theta) (>= 315 theta))) 0)
		(else (sqrt (* 2 size size (cos (* 2 theta)))))
	)
)  ; translated direction * sqrt(2 * size * cos(2 * theta)) the radius of an infinity based on angle

; converts polar (r, theta) coordinates to cartesian (x, y) and returns them as a list
(define (cartesian r theta)
	(list (* r (cos theta)) (* r (sin theta)))
)

(define (cadr lst)
	(car (cdr lst))
)

; note: cos, sin, sqrt, and pensize were all added to scheme_primitives for the purpose of the project
(draw)
