;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: 10 hours in Paint
;;;
;;; Description:
;;;   There are exactly
;;;    seventeen syllables in 
;;;    this recursive hai

(define (draw)
  ; *YOUR CODE HERE*
  (define (circle_hax d)
    (cond
      ((> d 30) (circle d) (left 5) (circle_hax (- d 1)))
    )
  )  
  (circle_hax 100)
  (exitonclick)
)

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)