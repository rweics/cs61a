;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Tree Recursion>
;;;
;;; Description:
;;;   <It's a glowing tree
;;;    More than a binary tree
;;;    Happy Holidays>

(define (tree length angle w k clr)
    (if (> k 0) 
        (begin
            (width w)
            (color (car clr))
            (circle length angle)
            (tree (/ length 1.5) (- angle 5) (/ w 1.3) (- k 1) (cdr clr))
            (color (car clr))
            (circle length (- angle))
            (circle (- length) angle)
            (color (car clr))
            (tree (/ length 1.5) (- angle 5) (/ w 1.3) (- k 1) (cdr clr))
            (color (car clr))
            (circle (- length) (- angle))
            (fd (* 3 length))
            (tree (/ length 1.5) (- angle 5) (/ w 1.3) (- k 1) (cdr clr))
            (color (car clr))
            (bk (* 3 length))
            (width (* w 1.2)))
        (begin 
            (width 1)
            (color "yellow")
            (circle 1))))

(define (repeat k fn)
    (if (> k 1)
        (begin (fn) (repeat (- k 1) fn))
        (fn)))

(define (draw)
    (speed 0) (hideturtle) (bgcolor "#020005")
    (pu) (goto 0 -300) (pd) (lt 90)
    (begin_fill) (color "white") (circle 600) (end_fill)
    (rt 90)
    (bk 10)
    (tree 60 35 9 7 (list "#734A12" "#6F5A12" "#6C6B12" "#516A0D" "#366809" "#185916" "#004023"))
    (pu) (fd 510) (lt 18) (bk 10) (pd) (width 1) (color "yellow") 
    (begin_fill) (repeat 5 (lambda () (fd 13) (lt 144) (fd 13) (lt 72))) (end_fill)
    (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
