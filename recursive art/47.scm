;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: What's Brown and Sticky?
;;;
;;; Description:
;;;		If it looks like this		
;;;		after you're done on the loo,
;;;		you should get that checked.

(define (draw)
	(speed 0)
	(draw-background)
	(pu) (setpos 300 -300) (seth 0) (pd) (eval-state (hilbert 6))
	(pu) (setpos 120 60) (seth -30) (pd) (eval-state (dragon 10))
	(pu) (setpos -170 -160) (seth 30) (pd) (eval-state (twig 6))
	(hideturtle)
  (exitonclick))

(define (lsys start rules)
	(lambda (n) (n-iters start rules n)))

(define (n-iters current rules n)
	(if (= n 0) 
		current 
		(n-iters (itersys nil current rules) rules (- n 1))))

(define (itersys prepend current rules)
	(if (not (null? current)) 
		(begin
			(define replacement (rules-modify (car current) rules))))
	(cond ((null? current) prepend)
				((null? rules) (append prepend current))
				((list? replacement)
					(itersys (append prepend replacement) (cdr current) rules))
				(else 
					(if (null? prepend) 
						(define prepend (list (car current)))
						(define prepend (append prepend (list (car current)))))
					(itersys prepend (cdr current) rules))))

(define (rules-modify symbol rules)
	(cond ((null? rules) #f)
				((eq? symbol (car (car rules))) (cdr (car rules)))
				(else (rules-modify symbol (cdr rules)))))

(define (eval-state state)
	(cond ((not (null? state)) 
		((eval (car state))) 
		(eval-state (cdr state)))))

(define (a) (color "brown") (pensize (max 1 (- 4 (length stack)))) (fd 3))
(define (b) (color "green") (fd 5))
(define (f) (set! greenlevel (op greenlevel .01))
						(if (> greenlevel .9) (set! op -))
						(if (< greenlevel .6) (set! op +))
						(color3 .5 greenlevel .5)
						(begin_fill)
						(fd 10) (left 90) 
						(fd 10) (left 90)
						(fd 10) (left 90) 
						(fd 10) (left 90)
						(end_fill)
						(fd 10))
(define (u) 
	(color3 0.6568627450980393 0.6882352941176471 0.6215686274509803) 
	(fd 10))
(define (l) (left 25))
(define (r) (right 25))
(define (p) (left 90))
(define (q) (right 90))
(define (x) nil)
(define (y) nil)

(define greenlevel .6)
(define op +)

(define stack nil)

(define (push x) 
	(set! stack (cons x stack)) 
	stack)
(define (pop)
	(define return (car stack)) 
	(set! stack (cdr stack)) 
	return)

(define (open) 
	(push (cons (getpos) (geth))))
(define (close)
	(define new (pop))
	(pu) (setpos (car (car new)) (cdr (car new))) (pd)
	(seth (cdr new)))

(define o open)
(define c close)

(define twig
	(lsys (list 'x) 
				(list (list 'x 'a 'l 'o 'o 'x 'c 'r 'x 'c 'r 'a 'o 'r 'a 'x 'c 'l 'x)
							(list 'a 'a 'a))))

(define dragon
	(lsys (list 'f 'x)
		(list (list 'x 'x 'p 'y 'f)
					(list 'y 'f 'x 'q 'y))))

(define hilbert
	(lsys (list 'x) 
		(list (list 'x 'p 'y 'u 'q 'x 'u 'x 'q 'u 'y 'p)
					(list 'y 'q 'x 'u 'p 'y 'u 'y 'p 'u 'x 'q))))

(define (draw-background)
	(pu) (setpos -400 -400) (pd)
	(pensize 30)
	(seth 90)
	(color3 0.68627 0.73725 0.39216)
	(background 30)
	(pensize 1))

(define (background steps)
	(cond ((> steps 0) 
		(color3 (+ (getr) (/ 0.24314 30)) 
						(+ (getg) (/ 0.2 30)) 
						(+ (getb) (/ 0.46666 30)))
		(fd 800) (back 800) (left 90) (fd 30) (right 90)
		(background (- steps 1)))))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)