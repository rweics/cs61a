;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: The Cipher
;;;
;;; Description:
;;;   This is just the start!
;;;   Unearth the hidden secret!
;;;   Become enlightened!

(define block-size 10)
(define code '(1 1 1 1 1 1 1 0 0 1 1 1 1 0 0 1 0 1 1 1 0 0 1 1 1 1 1 1 1 2 1 0 0 0 0 0 1 0 1 1 1 0 0 1 0 1 1 1 0 0 1 0 1 0 0 0 0 0 1 2 1 0 1 1 1 0 1 0 0 1 0 1 1 0 1 1 0 1 0 1 1 0 1 0 1 1 1 0 1 2 1 0 1 1 1 0 1 0 0 0 1 0 1 1 0 0 1 0 1 1 0 0 1 0 1 1 1 0 1 2 1 0 1 1 1 0 1 0 0 1 1 1 1 0 1 1 1 0 1 0 1 0 1 0 1 1 1 0 1 2 1 0 0 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 0 1 2 1 1 1 1 1 1 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1 1 1 1 1 1 1 2 0 0 0 0 0 0 0 0 1 0 1 0 0 1 1 0 1 1 1 1 1 0 0 0 0 0 0 0 0 2 0 0 0 0 1 1 1 1 0 1 0 0 1 1 0 1 0 0 1 0 0 0 1 1 0 0 0 1 0 2 0 1 0 0 1 0 0 1 1 0 0 0 0 1 1 1 0 0 0 1 1 0 0 1 1 0 0 1 1 2 1 0 1 1 1 1 1 1 1 0 0 0 1 0 0 1 0 0 0 0 1 0 0 1 1 1 1 0 1 2 1 0 0 0 0 0 0 0 1 1 0 0 1 0 0 0 0 0 0 0 0 0 0 1 1 1 0 1 1 2 1 0 1 0 0 0 1 0 0 0 1 0 1 1 1 0 1 1 1 1 1 0 0 0 0 1 0 0 1 2 1 0 0 1 0 1 0 1 0 0 0 0 0 1 0 0 0 1 0 0 0 0 1 0 1 0 1 0 1 2 1 0 0 0 0 0 1 1 0 1 0 1 0 0 1 1 1 0 1 0 1 0 0 1 1 0 0 1 1 2 1 0 0 1 1 1 0 0 1 0 1 0 0 0 0 0 1 0 1 1 1 1 1 1 1 1 0 1 1 2 1 1 0 0 0 1 1 0 1 0 1 0 1 1 1 1 1 1 1 0 1 1 0 0 1 0 0 1 1 2 1 1 0 0 1 1 0 0 1 0 0 1 1 1 0 0 0 1 0 1 0 1 0 1 1 1 1 0 1 2 0 0 0 1 1 1 1 1 1 0 1 0 0 1 1 1 0 1 0 1 0 0 0 0 0 1 0 0 1 2 0 0 0 0 0 1 0 1 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0 1 1 1 0 1 1 2 1 1 1 1 0 0 1 0 0 1 1 1 1 0 1 0 0 1 0 0 1 1 1 1 1 1 0 1 0 2 0 0 0 0 0 0 0 0 1 1 0 0 1 1 0 1 0 0 0 1 1 0 0 0 1 0 1 0 1 2 1 1 1 1 1 1 1 0 1 1 0 1 0 1 0 1 1 0 0 1 1 0 1 0 1 0 0 0 1 2 1 0 0 0 0 0 1 0 1 0 1 1 0 0 1 1 1 0 1 1 1 0 0 0 1 1 0 0 0 2 1 0 1 1 1 0 1 0 1 0 1 1 1 0 1 1 0 0 0 1 1 1 1 1 1 0 0 1 0 2 1 0 1 1 1 0 1 0 0 1 1 0 1 1 1 1 1 0 1 0 0 1 0 1 0 1 0 1 0 2 1 0 1 1 1 0 1 0 0 1 1 0 0 0 0 1 0 0 1 1 0 1 0 1 0 0 1 1 1 2 1 0 0 0 0 0 1 0 0 1 1 1 1 0 1 0 0 0 0 0 0 1 0 1 0 1 0 1 1 2 1 1 1 1 1 1 1 0 0 0 1 1 1 0 1 1 1 1 0 1 1 0 0 0 1 1 0 1 0 ))

(define (filled-square size)
	(begin_fill)
	(fd size)
	(right 90)
	(fd size)
	(right 90)
	(fd size)
	(right 90)
	(fd size)
	(right 90)
	(end_fill)
	(fd size))

(define (qr-step-new-line)
	(pu)
	(seth 180)
	(fd block-size)
	(seth 270)
	(fd (* 29 block-size))
	(seth 90))


(define (qr-draw code)
	(seth 90)
	(cond
		((null? code) 0)
		((= 1 (car code)) (begin (pd) (filled-square block-size) (qr-draw (cdr code))))
		((= 0 (car code)) (begin (pu) (fd block-size) (qr-draw (cdr code))))
		(else (begin (qr-step-new-line) (qr-draw (cdr code))))))

(define (draw)
  (speed 0)
  (pu)
  (define half-size (/ (* 29 block-size) 2))
  (goto (- half-size) half-size)
  (seth 90)
  (qr-draw code)
  (ht)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
