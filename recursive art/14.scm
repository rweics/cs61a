;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Kaleidoscope of Interference
;;;
;;; Description:
;;;   Infinite lines of blue and gold
;;;   interfere with each other,
;;;   yielding a beautiful high-order shape.

(define pi 3.1415926535897932384626433832795)
(define NUM_LINE 10)
(define SPAN (/ 400 NUM_LINE))
(define GOLD "#D4AB6D")
(define BLUE "#8FB4DE")

(define (dsin n)
  (sin (* n (/ pi 180))))

(define (dcos n)
  (cos (* n (/ pi 180))))

(define (draw-line-with-center cx cy ang)
  (goto
    (- cx (* 600 (dsin ang)))
    (- cy (* 600 (dcos ang))))
  (pendown)
  (fd 1200)
  (penup))

(define (next-x ang n)
  (- (* SPAN (dcos ang) n)))
(define (next-y ang n)
  (* SPAN (dsin ang) n))

(define (get-color n)
  (if (odd? n)
    GOLD
    BLUE))

(define (draw-group angle)
  (define (iter n)
    (if (< (* 2 n) NUM_LINE)
      (let ((nx (next-x angle n))
        (ny (next-y angle n)))
        (draw-line-with-center nx ny angle)
        (draw-line-with-center (- nx) (- ny) angle)
        (iter (+ n 1)))))
  (iter 0))

(define (draw-shape loop angle)
  (if (> loop 0)
    (begin
      (seth angle)
      (color (get-color loop))
      (draw-group angle)

    ;color
    (draw-shape (- loop 1) (- angle 5)))))

(define (draw)
  ; *YOUR CODE HERE*
  (speed 0)
  (ht)
  (draw-shape 40 112)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
