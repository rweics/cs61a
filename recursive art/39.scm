;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Lotus in scheme
;;;
;;; Description:
;;;   Parentheses of scheme
;;;   Blooms carefully in pairs
;;;   The only computer language that is beautiful.

(define (init)
  (seth 0)
  (pu)
  (goto 0 0)
  (pd)
 )

(define (bg)
  (color "black")
  (goto -320 -320)
  (clear)
  (pu)
  (begin_fill)
  (goto -320 320)
  (goto 320 320)
  (goto 320 -320)
  (end_fill)
  (goto -150 150)
  (pd)
  (color "#3366ee")
  (spiral 200 120)
  (pu)(goto 200 -200)(pd)
  (spiral 400 120))

(define c-set (list "#ff6666" "#ff7777" "#ff8888" "#ff9999" "#ffaaaa" "#ffbbbb" "#ffcccc"))
(define (k-th lst k)
  (if (zero? k)(car lst)
    (k-th (cdr lst) (- k 1))))

;Parentheses
(define (here-circle r deg)
  (define adjust (- 90 (/ deg 2)))
  (left adjust)
  (pu)(right 90)(fd r)(left 90)(pd)
  (circle r deg)
  (circle r (- deg))
  (pu)(left 90)(fd r)(right 90)(pd)
  (right adjust))

(define (spiral r n)
  (define (first step n)
    (if (< step n)
        (begin
        (circle (* step (/ r n)) 60)
        (first (+ 1 step) n))))
  (first 0 n)
)

(define (club r n)
  (define (first step n)
    (if (< step n)
        (begin
        (here-circle (* step (/ r n)) 55)
        (first (+ 1 step) n))))
  (define (second step n)
    (if (< step n)
        (begin
        (here-circle (+ r (* step (/ r n))) (* (- n step) (/ 55 n)))
        (second (+ 1 step) n))))
  (first 0 n)
  (second 0 n)
)

(define (circle-repeat func n)
    (define deg (/ 360 n))
    (define (helper func n deg)
      (if (> n 0)
      (begin (func)
       (right deg)
       (helper func (- n 1) deg))))
    (helper func n deg))

(define (grow n r)
    (define (petal) (club r (/ r 3)))
    (color (k-th c-set n))
    (if (> n 0)
      (begin (circle-repeat petal 6) (right 30) (grow (- n 1) (* r 0.618)))))
   

(define (draw)
  ; *YOUR CODE HERE*
  (speed 0)
  (ht)
  (bg)
  (init)
  (seth 20)
  (grow 6 150)
  (exitonclick))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)


